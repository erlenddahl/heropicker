# README #

A web page (made for both computers and smart phones) for picking a random hero for the game "Heroes of the Storm".

Built using AngularJS, and a tiny bit of PHP for getting the current free hero selection. The list of heroes is currently hard coded, but should probably be moved to separate .json files.

Can be seen live here: http://erlenddahl.no/etc/hots/