angular.module('hotsApp', [])

    .controller("HeroController", function($http, $scope, $timeout){

        var ctrl = this;

        ctrl.selectors = [
            {
                id: "role",
                name: "Role",
                selectors: [
                    {
                        id: "warrior",
                        name: "Warrior",
                        attribute: "role",
                        value: "Warrior",
                        isSelected: true
                    },
                    {
                        id: "assassin",
                        name: "Assassin",
                        attribute: "role",
                        value: "Assassin",
                        isSelected: true
                    },
                    {
                        id: "specialist",
                        name: "Specialist",
                        attribute: "role",
                        value: "Specialist",
                        isSelected: true
                    },
                    {
                        id: "support",
                        name: "Support",
                        attribute: "role",
                        value: "Support",
                        isSelected: true
                    }
                ]
            },
            {
                id: "universe",
                name: "Universe",
                selectors: [
                    {
                        id: "warcraft",
                        name: "Warcraft",
                        attribute: "universe",
                        value: "Warcraft",
                        isSelected: true
                    },
                    {
                        id: "starcraft",
                        name: "StarCraft",
                        attribute: "universe",
                        value: "StarCraft",
                        isSelected: true
                    },
                    {
                        id: "diablo",
                        name: "Diablo",
                        attribute: "universe",
                        value: "Diablo",
                        isSelected: true
                    },
                    {
                        id: "retro",
                        name: "Retro",
                        attribute: "universe",
                        value: "Retro",
                        isSelected: true
                    }
                ]
            },
            {
                id: "range",
                name: "Range",
                selectors: [
                    {
                        id: "ranged",
                        name: "Ranged",
                        attribute: "range",
                        value: "Ranged",
                        isSelected: true
                    },
                    {
                        id: "melee",
                        name: "Melee",
                        attribute: "range",
                        value: "Melee",
                        isSelected: true
                    }
                ]
            },
            {
                id: "ftp",
                name: "Free to play",
                selectors: [
                    {
                        id: "free",
                        name: "Free to play",
                        attribute: "ftp",
                        value: true,
                        isSelected: true
                    },
                    {
                        id: "notfree",
                        name: "Not 'Free to play'",
                        attribute: "ftp",
                        value: false,
                        isSelected: true
                    }
                ]
            }
        ];

        ctrl.heroes = [
            {
                id: "abathur",
                name: "Abathur",
                role: "Specialist",
                range: "Ranged",
                universe: "StarCraft",
                ftp: false
            },
            {
                id: "anubarak",
                name: "Anubarak",
                role: "Warrior",
                range: "Melee",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "artanis",
                name: "Artanis",
                role: "Warrior",
                range: "Melee",
                universe: "StarCraft",
                ftp: false
            },
            {
                id: "arthas",
                name: "Arthas",
                role: "Warrior",
                range: "Melee",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "azmodan",
                name: "Azmodan",
                role: "Specialist",
                range: "Ranged",
                universe: "Diablo",
                ftp: false
            },
            {
                id: "brightwing",
                name: "Brightwing",
                role: "Support",
                range: "Ranged",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "chen",
                name: "Chen",
                role: "Warrior",
                range: "Melee",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "cho",
                blizzId: "chogall",
                name: "Cho",
                role: "Warrior",
                range: "Melee",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "diablo",
                name: "Diablo",
                role: "Warrior",
                range: "Melee",
                universe: "Diablo",
                ftp: false
            },
            {
                id: "etc",
                name: "E.T.C.",
                role: "Warrior",
                range: "Melee",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "falstad",
                name: "Falstad",
                role: "Assassin",
                range: "Ranged",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "gall",
                blizzId: "chogall",
                name: "Gall",
                role: "Assassin",
                range: "Ranged",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "gazlowe",
                name: "Gazlowe",
                role: "Specialist",
                range: "Melee",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "greymane",
                name: "Greymane",
                role: "Assassin",
                range: "Ranged",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "illidan",
                name: "Illidan",
                role: "Assassin",
                range: "Melee",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "jaina",
                name: "Jaina",
                role: "Assassin",
                range: "Ranged",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "johanna",
                name: "Johanna",
                role: "Warrior",
                range: "Melee",
                universe: "Diablo",
                ftp: false
            },
            {
                id: "kaelthas",
                name: "Kael'thas",
                role: "Assassin",
                range: "Ranged",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "kerrigan",
                name: "Kerrigan",
                role: "Assassin",
                range: "Melee",
                universe: "StarCraft",
                ftp: false
            },
            {
                id: "kharazim",
                name: "Kharazim",
                role: "Support",
                range: "Melee",
                universe: "Diablo",
                ftp: false
            },
            {
                id: "leoric",
                name: "Leoric",
                role: "Warrior",
                range: "Melee",
                universe: "Diablo",
                ftp: false
            },
            {
                id: "li-li",
                name: "Li Li",
                role: "Support",
                range: "Ranged",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "lt-morales",
                name: "Lt. Morales",
                role: "Support",
                range: "Ranged",
                universe: "StarCraft",
                ftp: false
            },
            {
                id: "lunara",
                name: "Lunara",
                role: "Assassin",
                range: "Ranged",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "malfurion",
                name: "Malfurion",
                role: "Support",
                range: "Ranged",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "muradin",
                name: "Muradin",
                role: "Warrior",
                range: "Melee",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "murky",
                name: "Murky",
                role: "Specialist",
                range: "Melee",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "nazeebo",
                name: "Nazeebo",
                role: "Specialist",
                range: "Ranged",
                universe: "Diablo",
                ftp: false
            },
            {
                id: "nova",
                name: "Nova",
                role: "Assassin",
                range: "Ranged",
                universe: "StarCraft",
                ftp: false
            },
            {
                id: "raynor",
                name: "Raynor",
                role: "Assassin",
                range: "Ranged",
                universe: "StarCraft",
                ftp: false
            },
            {
                id: "rehgar",
                name: "Rehgar",
                role: "Support",
                range: "Melee",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "rexxar",
                name: "Rexxar",
                role: "Warrior",
                range: "Ranged",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "sgt-hammer",
                name: "Sgt. Hammer",
                role: "Specialist",
                range: "Ranged",
                universe: "StarCraft",
                ftp: false
            },
            {
                id: "sonya",
                name: "Sonya",
                role: "Warrior",
                range: "Melee",
                universe: "Diablo",
                ftp: false
            },
            {
                id: "stitches",
                name: "Stitches",
                role: "Warrior",
                range: "Melee",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "sylvanas",
                name: "Sylvanas",
                role: "Specialist",
                range: "Ranged",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "tassadar",
                name: "Tassadar",
                role: "Support",
                range: "Ranged",
                universe: "StarCraft",
                ftp: false
            },
            {
                id: "the-butcher",
                name: "The Butcher",
                role: "Assassin",
                range: "Melee",
                universe: "Diablo",
                ftp: false
            },
            {
                id: "the-lost-vikings",
                name: "The Lost Vikings",
                role: "Specialist",
                range: "Ranged",
                universe: "Retro",
                ftp: false
            },
            {
                id: "thrall",
                name: "Thrall",
                role: "Assassin",
                range: "Melee",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "tychus",
                name: "Tychus",
                role: "Assassin",
                range: "Ranged",
                universe: "StarCraft",
                ftp: false
            },
            {
                id: "tyrael",
                name: "Tyrael",
                role: "Warrior",
                range: "Melee",
                universe: "Diablo",
                ftp: false
            },
            {
                id: "tyrande",
                name: "Tyrande",
                role: "Support",
                range: "Ranged",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "uther",
                name: "Uther",
                role: "Support",
                range: "Melee",
                universe: "Warcraft",
                ftp: false
            },
            {
                id: "valla",
                name: "Valla",
                role: "Assassin",
                range: "Ranged",
                universe: "Diablo",
                ftp: false
            },
            {
                id: "zagara",
                name: "Zagara",
                role: "Specialist",
                range: "Ranged",
                universe: "StarCraft",
                ftp: false
            },
            {
                id: "zeratul",
                name: "Zeratul",
                role: "Assassin",
                range: "Melee",
                universe: "StarCraft",
                ftp: false
            }
        ];

        ctrl.hideUnselectedHeroes = false;
        ctrl.orBetweenGroups = false;

        ctrl.refreshFreeHeroes = function(){
            $http.get("free.php").
                then(function(response) {
                    try{
                        for(var i in ctrl.heroes){
                            ctrl.heroes[i].ftp = false;
                        }
                        for(var h in response.data){
                            var hero = response.data[h];
                            var found = false;
                            for(var i in ctrl.heroes){
                                if(ctrl.heroes[i].name === hero){
                                    console.log("Hero is ftp: " + hero);
                                    ctrl.heroes[i].ftp = true;
                                    found = true;
                                    break;
                                }
                            }

                            if(!found)
                                console.log("Couldn't find hero: '" + hero + "'");
                        }
                    }catch(err){
                        console.log(err);
                    }
                }, function(response) {
                    console.log(response);
                });
        }

        ctrl.refreshAvailableHeroes = function(){
            ctrl.availableHeroes = [];
            for(var h in ctrl.heroes){
                var hero = ctrl.heroes[h];
                if(hero.isSelected && !hero.isBanned)
                    ctrl.availableHeroes.push(hero);
            }
            ctrl.availableHeroesCount = ctrl.availableHeroes.length;
        };

        ctrl.refreshSelection = function(){
            var debug = false;

            if(debug)
                for(var s in ctrl.selectors){
                    console.log(ctrl.selectors[s].name);
                    for(var ss in ctrl.selectors[s].selectors){
                        var sel = ctrl.selectors[s].selectors[ss];
                        console.log("   > " + sel.name + ": " + sel.isSelected);
                    }
                }

            for(var h in ctrl.heroes){
                var hero = ctrl.heroes[h];
                var value = !ctrl.orBetweenGroups;

                if(debug) console.log(hero.name);

                for(var grp in ctrl.selectors){
                    var group = ctrl.selectors[grp];
                    var res = false;
                    for(var s in group.activeSelectors){
                        var sel = group.activeSelectors[s];
                        if(debug) console.log("   > " + sel.name + ": " + sel.isSelected + " (" + sel.value + " = " + hero[sel.attribute] + ")");
                        res = res || sel.value == hero[sel.attribute];
                    }
                    if(debug) console.log("      > " + res);
                    if(!ctrl.orBetweenGroups)
                        value = value && res;
                    else
                        value = value || res;
                    //if(!value) break; //No need to keep checking if it's already gone.
                }

                hero.isSelected = value;
            }

            ctrl.refreshAvailableHeroes();
        };

        ctrl.checkSelectors = function(group){
            var activeSelectors = [];
            for(var s in group.selectors){
                var sel = group.selectors[s];
                if(sel.isSelected)
                    activeSelectors.push(sel);
            }
            group.selectorCount = group.selectors.length;
            group.activeCount = activeSelectors.length;
            group.activeSelectors = activeSelectors;
        };

        ctrl.toggleAll = function(group){
            var val = group.selectorCount != group.activeCount;
            for(var s in group.selectors)
                group.selectors[s].isSelected = val;
            ctrl.checkSelectors(group);
            ctrl.refreshSelection();
        };

        ctrl.selectAll = function(group){
            for(var s in group.selectors)
                group.selectors[s].isSelected = true;
            ctrl.checkSelectors(group);
            ctrl.refreshSelection();
        };

        ctrl.select = function(group, selector){

            if(group && selector){
                selector.isSelected = !selector.isSelected;
                ctrl.checkSelectors(group);
            }

            ctrl.refreshSelection();
        };

        ctrl.pickRandomHero = function(){

            if($scope.picking) return;

            document.body.scrollTop = document.documentElement.scrollTop = 0;
            
            ctrl.refreshAvailableHeroes();

            $scope.pickedHero = null;
            $scope.picking = true;

            function pick(i) { 
                $scope.randomHero = ctrl.availableHeroes[Math.floor(Math.random()*ctrl.availableHeroes.length)];
                console.log($scope.randomHero);

                if(i < 25)
                    $timeout(function() { pick(++i); }, Math.max(i * 5, 100) + (i > 20 ? (i-20)*50 : 0));
                else{
                    $scope.pickedHero = $scope.randomHero;
                    $scope.picking = false;
                }
            }

            pick(0);

        };

        for(var g in ctrl.selectors){
            ctrl.selectAll(ctrl.selectors[g]);
        }

        ctrl.refreshFreeHeroes();
    });

